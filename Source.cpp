#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cstdio>
#include <string>
#include <set>
#include <algorithm>
#include <queue>

using namespace std;

typedef vector<vector<bool> > vvb;

class BitMap {

public:
	BitMap(char *filename) {
		initiate();

		FILE *img = fopen(filename, "rb");

		unsigned char header[54];
		fread(header, sizeof(unsigned char), 54, img);

		width = header[18];
		height = header[22];
		right = 0;
		left = width;
		bot = 0;
		top = height;

		image.resize(height, vector<bool>(width, false));
		visited.resize(height, vector<bool>(width, false));

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int r, g, b;
				b = getc(img);
				g = getc(img);
				r = getc(img);
				if (r == 0 && g == 0 && b == 0) {
					image[height - i - 1][j] = 1;
					bitCount++;
					if (start.first == -1)
						start = make_pair(height - 1 - i, j);
					if (left > j)
						left = j;
					if (right < j)
						right = j;
					if (top > i)
						top = i;
					if (bot < i)
						bot = i;
					centerX += j;
					centerY += height - i - 1;
				}
			}
			//getc(img);
		}
		centerX /= bitCount;
		centerY /= bitCount;
	}

	void PrintImage() {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				cout << (image[i][j] ? '1' : '0');
			}
			cout << endl;
		}
	}

	void getType() {
		bfs();
		//findAngles();
		//for (int i = 0; i < angles.size(); i++) {
		//	cout << angles[i].first << ' ' << angles[i].second << endl;
		//}
		countAngles();
		int anglesCnt = angles.size();

		if (circled) {
			getSides();
			if (anglesCnt == 3) {
				sort(sides.begin(), sides.end());
				if (approxEq(sqr(sides[0]) + sqr(sides[1]), sqr(sides[2]))) {
					cout << "Triangle with 90 angle\n";
				}
				if (approxEq(sides[0], sides[1]) && approxEq(sides[1], sides[2])) {
					cout << "Triangle, 3 equal sides!\n";
					return;
				}
				if (approxEq(sides[0], sides[1]) || approxEq(sides[1], sides[2])) {
					cout << "Triangle, 2 equal sides!\n";
					return;
				}


				cout << "Simple triangle\n";
				return;
			}
			if (anglesCnt == 4) {
				bool square = true;
				for (int i = 0; i < sides.size() - 1; i++) {
					if (!approxEq(sides[i], sides[i + 1]))
						square = false;
				}
				if (square)
					cout << "Square it is\n";
				else
					cout << "Rectangle\n";
				return;
			}
			if (anglesCnt == 0) {
				if (checkCircle()) {
					cout << "Circle\n";
					return;
				}
				cout << "Ellipse it is";
				return;
			}
		} else {
			if (anglesCnt == 0) {
				cout << "Line it is";
				return;
			} else {
				cout << "lomannaya\n";
				return;
			}
		}
	}

private:
	int width, height;
	vvb image;
	int bitCount = 0;
	int moveX[3];
	int moveY[3];
	vvb visited;
	bool circled;
	pair<int, int> start;

	int visitedBits = 0;

	void initiate() {
		moveX[0] = moveY[0] = -1;
		moveY[1] = moveX[1] = 0;
		moveX[2] = moveY[2] = 1;
	}

	vector<pair<int, int> > neihgbors(int x, int y) {
		vector<pair<int, int> > tmp;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				int newX = x + moveX[i];
				int newY = y + moveY[j];
				if (newX < 0 || newX >= height || newY < 0 || newY >= width)
					continue;
				if (image[newX][newY]) {
					tmp.push_back(make_pair(newX, newY));
				}
			}
		}
		return tmp;
	}

	bool isDeadEnd(int x, int y) {
		bool deadEnd = true;
		auto next = neihgbors(x, y);
		for (int i = 0; i < next.size(); i++) {
			if (!visited[next[i].first][next[i].second])
				deadEnd = false;
		}
		return deadEnd;
	}
	
	void bfs() {
		visited.assign(height, vector<bool>(width, false));
		queue<pair<int, int> > bfsq;
		bfsq.push(make_pair(0, 0));
		int moveX[4] = { 1, -1, 0, 0 };
		int moveY[4] = { 0, 0, -1, 1 };
		int counter = 0;

		visited[0][0] = true;
		while (!bfsq.empty()) {
			pair<int, int> cur = bfsq.front();
			bfsq.pop();
			counter++;
			for (int i = 0; i < 4; i++) {
				int x = cur.first + moveX[i];
				int y = cur.second + moveY[i];
				if (x < 0 || x >= width || y < 0 || y >= height)
					continue;
				if (!visited[x][y] && !image[x][y]) {
					bfsq.push(make_pair(x, y));
					visited[x][y] = true;
				}
			}
		}

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				cout << visited[i][j];
			}
			cout << endl;
		}

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (!visited[i][j] && !image[i][j]) {
					circled = true;
					return;
				}
			}
		}
		circled = false;
	}



	int left, top, right, bot;

	vector<string> baseMasks;
	void fillMasks() {
		baseMasks.push_back("001011000");
		baseMasks.push_back("010011000");
		baseMasks.push_back("011010000");
		//baseMasks.push_back("001010010");
		baseMasks.push_back("001010001");
		baseMasks.push_back("010010101");
		baseMasks.push_back("000100110");


		int n = baseMasks.size();
		for (int i = 0; i < n; i++) {
			string curMask = baseMasks[i];
			for (int j = 0; j < 3; j++) {
				char tmp = curMask[1];
				curMask[1] = curMask[5];
				curMask[5] = curMask[7];
				curMask[7] = curMask[3];
				curMask[3] = tmp;
				tmp = curMask[0];
				curMask[0] = curMask[2];
				curMask[2] = curMask[8];
				curMask[8] = curMask[6];
				curMask[6] = tmp;
				baseMasks.push_back(curMask);
			}
		}
	}

	double centerX = 0, centerY = 0;

	void countAngles() {
		vector<pair<int, int> > tmp;
		fillMasks();
		int count = 0;
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				string mask;
				for (int k = 0; k <= 2; k++) {
					for (int l = 0; l <= 2; l++) {
						int newI = i + moveX[k];
						int newJ = j + moveY[l];
						if (newI < 0 || newI >= height || newJ < 0 || newJ >= width) {
							mask += '0';
							continue;
						}
						if (image[newI][newJ])
							mask += '1';
						else
							mask += '0';
					}
				}
				for (int k = 0; k < baseMasks.size(); k++) {
					if (baseMasks[k] == mask) {
						//printf("%d, %d - angle\n", i, j);
						tmp.push_back(make_pair(i, j));
					}
				}
			}
		}
		for (int i = 0; i < tmp.size(); i++) {
			bool fl = true;
			for (int j = i + 1; j < tmp.size(); j++) {
				int x1 = tmp[i].first;
				int y1 = tmp[i].second;
				int x2 = tmp[j].first;
				int y2 = tmp[j].second;
				if (abs(x1 - x2) <= 1 && abs(y1 - y2) <= 1)
					fl = false;
			}
			if (fl) {
				angles.push_back(tmp[i]);
				cout << tmp[i].first << ' ' << tmp[i].second << " - Angle\n";
			}
		}
	}

	vector<pair<int, int> > angles;
	//	triangles, rectangles
	void findAngles() {
		int tl = 0, bl = 0, tr = 0, br = 0;
		bool tlFound = false, blFound = false;
		for (int i = left; i <= right; i++) {
			if (image[i][top] && !tlFound) {
				tl = i;
				tlFound = true;
				angles.push_back(make_pair(top, tl));
			}
			if (image[i][top]) {
				tr = i;
			}
		}
		if (tr - tl > 1)
			angles.push_back(make_pair(top, tr));
		for (int i = left; i <= right; i++) {
			if (image[i][top] && !blFound) {
				bl = i;
				blFound = true;
				angles.push_back(make_pair(bot, bl));
			}
			if (image[i][top]) {
				br = i;
			}
		}
		if (br - bl > 1)
			angles.push_back(make_pair(bot, br));
	}

	vector<double> sides;

	double sqr(double x) {
		return x * x;
	}

	double getSide(pair<int, int> x, pair<int, int> y) {
		return sqrt(sqr(x.first - y.first) + sqr(x.second - y.second));
	}

	void getSides() {
		if (angles.size() == 3) {
			sides.push_back(getSide(angles[0], angles[1]));
			sides.push_back(getSide(angles[1], angles[2]));
			sides.push_back(getSide(angles[2], angles[0]));
		}
		if (angles.size() == 4) {
			sides.push_back(getSide(angles[0], angles[1]));
			sides.push_back(getSide(angles[1], angles[3]));
			sides.push_back(getSide(angles[3], angles[2]));
			sides.push_back(getSide(angles[2], angles[0]));
		}
		for (int i = 0; i < sides.size(); i++) {
			cout << sides[i] << endl;
		}
	}

	double eps = .1;
	double approxEq(double x, double y) {
		bool expr = max(x, y) - min(x, y) < max(x, y) * eps;
		return expr;
	}

	double checkCircle() {
		printf("Center: %f %f\n", centerX, centerY);
		bool circle = true;
		double radius = getSide(start, make_pair(centerX, centerY));
		printf("radius: %f\n", radius);
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (image[i][j])
					if (!approxEq(radius, getSide(make_pair(centerX, centerY), make_pair(i, j)))) {
						//cout << getSide(make_pair(centerX, centerY), make_pair(i, j)) << endl;
						//cout << start.first << ' ' << start.second << endl;
						circle = false;
					}
			}
		}
		return circle;
	}
};

int main() {

	BitMap shape("triangletup.bmp");
	shape.PrintImage();
	shape.getType();
	return 0;
}